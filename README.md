# README #

### What is this repository for? ###

This is a public repo that contains the Xenome binaries from the 2012 Bioinformatics manuscript (https://bioinformatics.oxfordjournals.org/content/28/12/i172.full). I, nor anyone at the Van Andel Institute have modified the original code in any way. This software is distributed without warranty and with the authors' original license of a CC-BY-NC 3.0.
